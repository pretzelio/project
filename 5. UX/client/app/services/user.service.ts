import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  register(user): Observable<any> {
    return this.http.post('/api/user', JSON.stringify(user), this.options);
  }

  login(credentials): Observable<any> {
    return this.http.post('/api/login', JSON.stringify(credentials), this.options);
  }

  getUsers(): Observable<any> {
    return this.http.get('/api/users').map(res => res.json());
  }

  countUsers(): Observable<any> {
    return this.http.get('/api/users/count').map(res => res.json());
  }

  addUser(user): Observable<any> {
    return this.http.post('/api/user', JSON.stringify(user), this.options);
  }

  getUser(user): Observable<any> {
    return this.http.get(`/api/user/${user._id}`).map(res => res.json());
  }

  getUserId(id): Observable<any> {
    return this.http.get(`/api/user/${id}`).map(res => res.json());
  }

  editUser(user): Observable<any> {
    return this.http.put(`/api/user/${user._id}`, JSON.stringify(user), this.options);
  }

  deleteUser(user): Observable<any> {
    return this.http.delete(`/api/user/${user._id}`, this.options);
  }

  addClient(id,client): Observable<any> {
    return this.http.put(`/api/user/${id}/client`, JSON.stringify(client), this.options);
  }

  editClient(id,client): Observable<any> {
    return this.http.put(`/api/user/${id}/client/${client._id}`, JSON.stringify(client), this.options);
  }

  deleteClient(id,client): Observable<any> {
    return this.http.delete(`/api/user/${id}/client/${client._id}`, this.options);
  }

  addClass(id,exercise_new): Observable<any> {
    return this.http.put(`/api/user/${id}/classes`, JSON.stringify(exercise_new), this.options);
  }

  editClass(id,exercise): Observable<any> {
    return this.http.put(`/api/user/${id}/classes/${exercise._id}`, JSON.stringify(exercise), this.options);
  }

  deleteClass(id,exercise): Observable<any> {
    return this.http.delete(`/api/user/${id}/classes/${exercise._id}`, this.options);
  }
}
