import { Component, OnInit } from '@angular/core';
import { ToastComponent } from '../shared/toast/toast.component';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import {FormGroup, FormControl, Validators, FormBuilder, FormArray} from '@angular/forms';
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-shedule',
  templateUrl: './shedule.component.html',
  styleUrls: ['./shedule.component.sass']
})

export class SheduleComponent implements OnInit {

  isLoading = true;
  user = {};
  exercise = {};
  client = {};
  addClientForm: FormGroup;
  id = new FormControl('');
  name = new FormControl('', [Validators.required,Validators.minLength(2),Validators.pattern('[a-zA-Zа-яА-Я-\\s]*')]);
  lastname = new FormControl('',[Validators.required,Validators.minLength(3),Validators.pattern('[a-zA-Zа-яА-Я-]*')]);
  number = new FormControl('', [Validators.required,Validators.minLength(7),Validators.maxLength(20),Validators.pattern('[0-9-\\s]*')]);
  email = new FormControl('',[Validators.required,Validators.pattern('^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$')]);

  addClassForm: FormGroup;
  id_class = new FormControl('');
  class_name = new FormControl('',[Validators.required,Validators.minLength(4),Validators.pattern('[a-zA-Zа-яА-Я-\\s]*')]);
  start_date = new FormControl('', [Validators.required]);
  end_date = new FormControl('', [Validators.required]);
  duration = new FormControl('', [Validators.required,Validators.minLength(2),Validators.pattern('[a-zA-Zа-яА-Я0-9.-\\s]*')]);
  time_class = new FormControl('', [Validators.required,Validators.minLength(4),Validators.pattern('[0-9-:.]*')]);
  client_name = new FormControl('', Validators.required);

  checks = [
    {description: 'Понедельник', value: '1'},
    {description: "Вторник", value: '2'},
    {description: "Среда", value: '3'},
    {description: "Четверг", value: '4'},
    {description: "Пятница", value: '5'},
    {description: "Суббота", value: '6'},
    {description: "Воскресенье", value: '0'}
  ];

  constructor(public auth: AuthService,
              private userService: UserService,
              public toast: ToastComponent,
              private formBuilder: FormBuilder) {}

  ngOnInit() {

    this.getUser();

    this.addClientForm = this.formBuilder.group({
      id: this.id,
      name: this.name,
      lastname: this.lastname,
      number: this.number,
      email: this.email
    });

    this.addClassForm = this.formBuilder.group({
      id_class: this.id_class,
      class_name: this.class_name,
      start_date: this.start_date,
      end_date: this.end_date,
      duration: this.duration,
      choices: new FormArray([]),
      time_class: this.time_class,
      client_name: this.client_name
    });

    }

  setClientName() {
    return { 'has-danger': !this.name.pristine && !this.name.valid };
  }
  setClientNumber() {
    return { 'has-danger': !this.number.pristine && !this.number.valid };
  }
  setClientEmail() {
    return { 'has-danger': !this.email.pristine && !this.email.valid };
  }
  setClientLastname() {
    return { 'has-danger': !this.lastname.pristine && !this.lastname.valid };
  }


  setClassName() {
    return { 'has-danger': !this.class_name.pristine && !this.class_name.valid };
  }
  setClassDuration() {
    return { 'has-danger': !this.duration.pristine && !this.duration.valid };
  }
  setClassTime() {
    return { 'has-danger': !this.time_class.pristine && !this.time_class.valid };
  }


  onCheckChange(event) {
    const formArray: FormArray = this.addClassForm.get('choices') as FormArray;
    /* Selected */
    if(event.target.checked){
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else{
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: FormControl) => {
        if(ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  getUser() {
    this.userService.getUser(this.auth.currentUser).subscribe(
        data => this.user = data,
        error => console.log(error),
        () => this.isLoading = false
    );
  }

  addClient(user) {
    this.userService.addClient(user._id,this.addClientForm.value).subscribe(
        res => {
          const newClient = res.json();
          this.user['clients'].push(newClient);
          this.addClientForm.reset();
          this.toast.setMessage('Клиент добавлен', 'success');
        },
        error => console.log(error)
    );
  }

  enableEditing(client) {
    this.client = client;
  }

  editClient(user, client) {
    this.userService.editClient(user._id, client).subscribe(
        res => {
          this.client = client;
          this.toast.setMessage('Информация о клиенте изменена', 'success');
        },
        error => console.log(error)
    );
  }

  enableDeleting(client) {
    this.client = client;
  }

  deleteClient(user,client) {
    this.userService.deleteClient(user._id,client).subscribe(
      data => this.toast.setMessage('Клиент удалён', 'success'),
      error => console.log(error),
      () => this.getUser()
    );
  }

  addClass(user) {
    var formatter= new Intl.DateTimeFormat("ru");
    const name_class = this.addClassForm.value.class_name;
    let date_start = this.addClassForm.value.start_date;
    let date_end = this.addClassForm.value.end_date;
    const duration_class = this.addClassForm.value.duration;
    const class_time = this.addClassForm.value.time_class;
    const client_class = this.addClassForm.value.client_name;
    let array  = this.addClassForm.value.choices;
    let i_date,n_array;
    let exercise_new = {};
    let date_class;
    let added_exercises=0;
    for(i_date= new Date(date_start); i_date<=new Date(date_end); i_date.setDate(i_date.getDate()+1)) {
      if (array.length==0){
        added_exercises++;
        date_class = formatter.format(i_date);
        exercise_new = {
          'name': name_class,
          'time': class_time,
          'date': date_class,
          'duration': duration_class,
          'client': client_class
        };
        this.forClass(user._id,exercise_new);
      }
      else {
        for (n_array = 0; n_array < array.length; n_array++) {//перебор значений массива чекбоксов
          if (i_date.getDay() == array[n_array]) {
            added_exercises++;
            date_class = formatter.format(i_date);
            exercise_new = {
              'name': name_class,
              'time': class_time,
              'date': date_class,
              'duration': duration_class,
              'client': client_class
            };
            this.forClass(user._id,exercise_new);
          }
        }
      }
    }
    if (added_exercises==0){
      this.toast.setMessage('Ни одно занятие не было добавлено, выберите другие даты или дни недели', 'success');
    }
  }

  forClass(id,exercise_new){
    this.userService.addClass(id, exercise_new).subscribe(
        res => {
          const newExercise = res.json();
          this.user['classes'].push(newExercise);
          this.addClassForm.reset();
          this.toast.setMessage('Занятие добавлено', 'success');
        },
        error => console.log(error)
    );
  }

  enableEditingExer(exercise) {
    this.exercise = exercise;
  }

  editClass(user, exercise) {
    this.userService.editClass(user._id, exercise).subscribe(
        res => {
          this.exercise = exercise;
          this.toast.setMessage('Информация о занятии изменена', 'success');
        },
        error => console.log(error)
    );
  }

  enableDeletingExer(exercise) {
    this.exercise = exercise;
  }

  deleteClass(user,exercise) {
    this.userService.deleteClass(user._id,exercise).subscribe(
        data => this.toast.setMessage('Занятие удалено', 'success'),
        error => console.log(error),
        () => this.getUser()
    );
  }


}
