import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-trainers-id',
  templateUrl: 'trainers-id.component.html',
  styleUrls: ['trainers-id.component.sass']
})

export class TrainersIdComponent implements OnInit, OnDestroy {
    id: any;
    private sub: any;
    trainer = {};
    isLoading = true;

    constructor(private route: ActivatedRoute,
                private userService: UserService) {}

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = params['id']; 
           this.getUser();
        });
    }

    getUser() {
        this.userService.getUserId(this.id).subscribe(
            data => this.trainer = data,
            error => console.log(error),
            () => this.isLoading = false
        );
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}