import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import * as _ from 'underscore';
import { PagerService } from '../services/pager.service'


@Component({
    moduleId: module.id,
  selector: 'app-trainers',
  templateUrl: './trainers.component.html',
  styleUrls: ['./trainers.component.sass']
})

export class TrainersComponent implements OnInit {
  isLoading = true;

  constructor(private userService: UserService,
              private http: Http,
              private pagerService: PagerService) { }

    private trainers: any[]; // все тренеры

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(
        data => {this.trainers = data;
            this.setPage(1);},
        error => console.log(error),
        () => this.isLoading = false
    );
  }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.pagerService.getPager(this.trainers.length, page);

        // get current page of items
        this.pagedItems = this.trainers.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

}
