import * as bodyParser from 'body-parser'; //обрабатывает тела запросов и выставляет для них req.body
import * as dotenv from 'dotenv'; // загружает переменные окружения из .env в process.env
import * as express from 'express';
import * as morgan from 'morgan'; //для регистрации деталей запроса
import * as mongoose from 'mongoose';
import * as path from 'path'; // предоставляет утилиты для работы с путями к файлам и директориям

import setRoutes from './routes';

const app = express();
dotenv.load({ path: '.env' });
app.set('port', (process.env.PORT || 3000));

app.use('/', express.static(path.join(__dirname, '../public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(morgan('dev'));
mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;
(<any>mongoose).Promise = global.Promise;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');

  setRoutes(app);

  app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '../public/index.html'));
  });

  app.listen(app.get('port'), () => {
    console.log('Angular listening on port ' + app.get('port'));
  });

});

export { app };
