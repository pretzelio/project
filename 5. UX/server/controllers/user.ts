import * as jwt from 'jsonwebtoken';

import User from '../models/user';
import BaseCtrl from './base';

export default class UserCtrl extends BaseCtrl {
  model = User;

  login = (req, res) => {
    this.model.findOne({ email: req.body.email }, (err, user) => { //поиск пользователя в БД
      if (!user) { return res.sendStatus(403); } //отказ в авторизации
      user.comparePassword(req.body.password, (error, isMatch) => {
        if (!isMatch) { return res.sendStatus(403); }
        const token = jwt.sign({ user: user }, process.env.SECRET_TOKEN);
          res.status(200).json({ token: token });
      });
    });
  }

    addClient = (req, res) => {
        this.model.update(
            {_id: req.params.id},
            {
                $push: {
                    'clients': {
                        'name': req.body.name,
                        'lastname': req.body.lastname,
                        'number': req.body.number,
                        'email': req.body.email
                    }
                }
            },
            (err) => {
                if (err) {
                    return console.error(err);
                }
                res.status(200).json(req.body);
            }
        );
    }

    updateClient = (req, res) => {
        this.model.update(
            {_id: req.params.id, "clients._id": req.params.idc},
            {
                $set: {
                    'clients.$.name': req.body.name,
                    'clients.$.lastname': req.body.lastname,
                    'clients.$.number': req.body.number,
                    'clients.$.email': req.body.email
                }
            },
            (err) => {
                if (err) {
                    return console.error(err);
                }
                res.sendStatus(200);
            }
        );
    }

    deleteClient = (req, res) => {
        this.model.update({ _id: req.params.id},
            {
                $pull: {
                    clients:{
                        _id: req.params.idc
                    }
                }
            },
            (err) => {
                if (err) { return console.error(err); }
                res.sendStatus(200);
            });
    }

    addClass = (req, res) => {
        this.model.update(
            {_id: req.params.id},
            {
                $push: {
                    'classes': {
                        'name': req.body.name,
                        'date': req.body.date,
                        'time': req.body.time,
                        'duration': req.body.duration,
                        'client': req.body.client
                    }
                }
            },
            (err) => {
                if (err) {
                    return console.error(err);
                }
                res.status(200).json(req.body);
            }
        );
    }

    updateClass = (req, res) => {
        this.model.update(
            {_id: req.params.id, "classes._id": req.params.idc},
            {
                $set: {
                    'classes.$.name': req.body.name,
                    'classes.$.time': req.body.time,
                    'clients.$.duration': req.body.duration
                }
            },
            (err) => {
                if (err) {
                    return console.error(err);
                }
                res.sendStatus(200);
            }
        );
    }

    deleteClass = (req, res) => {
        this.model.update({ _id: req.params.id},
            {
                $pull: {
                    classes:{
                        _id: req.params.idc
                    }
                }
            },
            (err) => {
                if (err) { return console.error(err); }
                res.sendStatus(200);
            });
    }

}
