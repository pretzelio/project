import * as express from 'express';

import UserCtrl from './controllers/user';

export default function setRoutes(app) {

  const router = express.Router();

  const userCtrl = new UserCtrl();

  router.route('/login').post(userCtrl.login);
  router.route('/users').get(userCtrl.getAll);
  router.route('/users/count').get(userCtrl.count);
  router.route('/user').post(userCtrl.insert);
  router.route('/user/:id').get(userCtrl.get);
 
  router.route('/user/:id').put(userCtrl.update);
  router.route('/user/:id').delete(userCtrl.delete);

  router.route('/user/:id/client').put(userCtrl.addClient);
  router.route('/user/:id/client/:idc').put(userCtrl.updateClient);
  router.route('/user/:id/client/:idc').delete(userCtrl.deleteClient);

  router.route('/user/:id/classes').put(userCtrl.addClass);
  router.route('/user/:id/classes/:idc').put(userCtrl.updateClass);
  router.route('/user/:id/classes/:idc').delete(userCtrl.deleteClass);

  // добавляем роутер с префиксом /api
  app.use('/api', router);

}
